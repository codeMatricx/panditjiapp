package com.example.vaibhav.tabpanditji.YogaListResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YogaList {
    @SerializedName("yogaId")
    @Expose
    public String yogaId;
    @SerializedName("yogaName")
    @Expose
    public String yogaName;

}