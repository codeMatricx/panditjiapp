package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.Adapter.MantrasList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.Katha_Request;
import com.example.vaibhav.tabpanditji.RequestParser.Katha_Requestparser;
import com.example.vaibhav.tabpanditji.ResponseParser.Katha_Response;
import com.example.vaibhav.tabpanditji.ResponseParser.MantrasList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class KathaDetails extends AppCompatActivity {

    TextView name,desc;
    ImageView imges;
    Toolbar mtoolbar_top;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katha_details);
        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        name=(TextView)findViewById(R.id.tv_katahname);
        desc=(TextView)findViewById(R.id.tv_kathadesc);
        imges=(ImageView)findViewById(R.id.img_katha);
        String nameofkatha=CommonMethod.getsavekathaname(KathaDetails.this);
        name.setText(nameofkatha);
        new Katahdetails().execute();
    }

    private class Katahdetails extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Katha_Response mkatha_response;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(KathaDetails.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mkatha_response = gson.fromJson(response, Katha_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mkatha_response != null) {
                if (mkatha_response.responseCode.trim().equals("200")) {
                    //String kathaimage=mkatha_response.getKatha.image;
                    String kathadescp=mkatha_response.getKatha.katha;
                    desc.setText(kathadescp);
                    //imges.setImageResource(Integer.parseInt(kathaimage));

                } else {
                    CommonMethod.showAlert(mkatha_response.responseMessage.toString().trim(), KathaDetails.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), KathaDetails.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.GetKathaDesc;
            Log.e("url----------------", "" + url);
            try {
                String kathaides=CommonMethod.getsavekathaid(KathaDetails.this);
                Katha_Request mkatha_request = new Katha_Request();
                mkatha_request.kathaId=kathaides;
                Gson gson = new Gson();
                String requestInString = gson.toJson(mkatha_request, Katha_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(KathaDetails.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
