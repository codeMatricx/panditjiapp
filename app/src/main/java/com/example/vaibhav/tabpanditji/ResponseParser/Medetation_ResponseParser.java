package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 2/3/2018.
 */

public class Medetation_ResponseParser {
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("meditationList")
    @Expose
    public List<MeditationList> meditationList;
}
