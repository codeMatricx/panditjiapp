package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 2/3/2018.
 */

public class HomeRemediesList {
    @SerializedName("homeRemediesId")
    @Expose
    public String homeRemediesId;
    @SerializedName("homeRemediesName")
    @Expose
    public String homeRemediesName;
}
