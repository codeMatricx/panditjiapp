package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.vaibhav.tabpanditji.Adapter.AartiList_Adapter;
import com.example.vaibhav.tabpanditji.Adapter.ChalishaList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.AartiList_RequestParser;
import com.example.vaibhav.tabpanditji.RequestParser.ChalishaList_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList_ResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.ChalishaList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class Chalisa_List extends AppCompatActivity {

    ListView chalisha_list;

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar myActionBar= getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_chalisa__list);
        chalisha_list=(ListView)findViewById(R.id.chalisa_list_id);

        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);

        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        chalisha_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    Intent intent = new Intent(Chalisa_List.this, ChalisaActivity.class);
                    startActivity(intent);
                }
            }
        });

        new NewChalisaList().execute();
    }

    private class NewChalisaList extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private ChalishaList_ResponseParser chalishaList_responseParser;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Chalisa_List.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                chalishaList_responseParser = gson.fromJson(response,ChalishaList_ResponseParser .class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (chalishaList_responseParser != null) {
                if (chalishaList_responseParser.responseCode.trim().equals("200")) {
                    ChalishaList_Adapter mAll_Adapter = new ChalishaList_Adapter(Chalisa_List.this, chalishaList_responseParser.chalisaList);
                    chalisha_list.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();
                } else {
                    CommonMethod.showAlert(chalishaList_responseParser.responseMessage.toString().trim(), Chalisa_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Chalisa_List.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.CHALISALIST;
            Log.e("url----------------", "" + url);
            try {
                ChalishaList_RequestParser chalishaList_requestParser = new ChalishaList_RequestParser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(chalishaList_requestParser, AartiList_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Chalisa_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

