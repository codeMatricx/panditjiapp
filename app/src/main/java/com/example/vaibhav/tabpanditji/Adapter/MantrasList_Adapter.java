package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.MantraList;

import java.util.List;

/**
 * Created by kumar on 2/3/2018.
 */

public class MantrasList_Adapter extends BaseAdapter
{
    private Context mContext;

    private List<MantraList> mantraList;

    public MantrasList_Adapter(Context mContext,List<MantraList> mantraList)
    {
        this.mContext = mContext;
        this.mantraList = mantraList;
    }

    @Override
    public int getCount() {
        return mantraList.size();
    }

    @Override
    public Object getItem(int position) {
        return mantraList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MantrasList_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_mantra__list1, null);
            holder = new MantrasList_Adapter.Holder();
            holder.name=(TextView)convertView.findViewById(R.id.tv_mantraList_id);


            convertView.setTag(holder);
        }
        else
        {
            holder = (MantrasList_Adapter.Holder) convertView.getTag();
        }

        holder.name.setText(mantraList.get(position).mantraListName);

        return convertView;

    }
    public class Holder {
        TextView name;

    }
}
