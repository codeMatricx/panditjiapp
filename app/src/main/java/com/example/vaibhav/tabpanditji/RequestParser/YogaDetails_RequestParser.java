package com.example.vaibhav.tabpanditji.RequestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/5/2018.
 */

public class YogaDetails_RequestParser
{
    @SerializedName("yogaId")
    @Expose
    public String yogaId;
}
