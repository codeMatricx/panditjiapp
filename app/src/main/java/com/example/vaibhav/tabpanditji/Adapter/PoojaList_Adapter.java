package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList;
import com.example.vaibhav.tabpanditji.ResponseParser.PoojaList;

import java.util.List;

/**
 * Created by kumar on 2/3/2018.
 */

public class PoojaList_Adapter extends BaseAdapter
{
    private Context mContext;

    private List<PoojaList> poojaList;

    public PoojaList_Adapter(Context mContext, List<PoojaList> poojaList)
    {
        this.mContext = mContext;
        this.poojaList = poojaList;
    }

    @Override
    public int getCount() {
        return poojaList.size();
    }

    @Override
    public Object getItem(int position) {
        return poojaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        PoojaList_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_pooja__list1, null);
            holder = new PoojaList_Adapter.Holder();
            holder.name=(TextView)convertView.findViewById(R.id.tv_poojaList_id);


            convertView.setTag(holder);
        }
        else
        {
            holder = (PoojaList_Adapter.Holder) convertView.getTag();
        }

        holder.name.setText(poojaList.get(position).poojaName);

        return convertView;

    }
    public class Holder {
        TextView name;

    }
}

