package com.example.vaibhav.tabpanditji.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class CommonMethod {

    private static Editor editor;
    public static String NOTIFICATION_STATUS="NOTIFICATION_STATUS";

    /**
     * Called for setting the value based on key in Prefs
     */

    public static void savekathaname(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Kathaname", value);
        editor.commit();

    }
    public static String getsavekathaname(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Kathanames = sharedPreferences.getString("Kathaname", "");
        return Kathanames;

    }
    //--------------------------------------------------
    public static void savekathaid(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Katahid", value);
        editor.commit();

    }
    public static String getsavekathaid(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String KAtahides = sharedPreferences.getString("Katahid", "");
        return KAtahides;

    }
    //--------------------------------------------------
    public static void savepoojaname(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PoojaName", value);
        editor.commit();

    }
    public static String getsavepoojaname(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String POOJANAME = sharedPreferences.getString("PoojaName", "");
        return POOJANAME;

    }
    //--------------------------------------------------
    public static void saveyogalistides(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("E_userid", value);
        editor.commit();

    }
    public static String getsaveUseridyogalist(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Euserid = sharedPreferences.getString("E_userid", "");
        return Euserid;

    }
    //--------------------------------------------------
    public static void savyogalistname(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Listname", value);
        editor.commit();

    }
    public static String getsavyogalistname(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Lname = sharedPreferences.getString("Listname", "");
        return Lname;

    }
//=========================================================

    public static void savePoojavidhi(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Vidhiid", value);
        editor.commit();

    }
    public static String getsavePoojavidhi(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String VidhiIdes = sharedPreferences.getString("Vidhiid", "");
        return VidhiIdes;

    }



    /**
     * Called for getting the value based on key from Prefs
     */
    public static String getPrefsData(Context context, String prefsKey,
                                      String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String prefsValue = sharedPreferences.getString(prefsKey, defaultValue);
        return prefsValue;
    }

    /**
     * Called for Showing Alert in Application
     */
    public static void showAlert(String message, Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Called for checking Internet connection
     */
    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo;
        try {
            netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }





//==========================================================================
}