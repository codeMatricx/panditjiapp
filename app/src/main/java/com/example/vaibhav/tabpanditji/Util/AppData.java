package com.example.vaibhav.tabpanditji.Util;


import android.graphics.Bitmap;


public class AppData {

	Bitmap thumbnailMartyrInfo = null;
	Bitmap thumbnailPostCreater = null;

	public Bitmap getThumbnailPostCreater() {
		return thumbnailPostCreater;
	}

	public void setThumbnailPostCreater(Bitmap thumbnailPostCreater) {
		this.thumbnailPostCreater = thumbnailPostCreater;
	}


	private static AppData singletonObject;

	public static AppData getSingletonObject() {
		if (singletonObject == null) {
			singletonObject = new AppData();
		}
		return singletonObject;
	}

}
