package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.ChalisaList;

import java.util.List;

/**
 * Created by kumar on 2/5/2018.
 */

public class ChalishaList_Adapter extends BaseAdapter
{
    private Context mContext;

    private List<ChalisaList> chalisaList;

    public ChalishaList_Adapter(Context mContext,List<ChalisaList> chalisaList)
    {
        this.mContext = mContext;
        this.chalisaList = chalisaList;
    }

    @Override
    public int getCount() {
        return chalisaList.size();
    }

    @Override
    public Object getItem(int position) {
        return chalisaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ChalishaList_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_chalisa__list1, null);
            holder = new ChalishaList_Adapter.Holder();
            holder.name=(TextView)convertView.findViewById(R.id.tv_chalisalist);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ChalishaList_Adapter.Holder) convertView.getTag();
        }

        holder.name.setText(chalisaList.get(position).chalisaName);

        return convertView;

    }
    public class Holder {
        TextView name;

    }
}
