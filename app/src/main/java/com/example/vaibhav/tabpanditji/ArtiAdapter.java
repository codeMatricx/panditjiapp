package com.example.vaibhav.tabpanditji;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by vaibhav on 3/1/18.
 */

public class ArtiAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] Arti_desc;
    private final Integer[] ArtiImage;

    public ArtiAdapter(Activity context, Integer[] artiImage,String[] arti_desc) {
        super(context,R.layout.layout_arti,arti_desc);
        this.context = context;
        Arti_desc = arti_desc;
        ArtiImage = artiImage;
    }
    public View getView(int position, View view, ViewGroup parent){




        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.layout_arti, null,true);

        TextView desc= (TextView) rowView.findViewById(R.id.tv_arti);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.Iv_arti);



        desc.setText(Arti_desc[position]);
        // txttime.setText(times[position]);
        imageView.setImageResource(ArtiImage[position]);
        return rowView;

    }
}
