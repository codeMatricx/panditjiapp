package com.example.vaibhav.tabpanditji;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by iSiwal on 1/5/2018.
 */

public class MyAppAdapter extends BaseAdapter {


    public List<Data> parkingList;
    public Context context;
    public static ViewHolder viewHolder;

    MyAppAdapter(List<Data> apps, Context context)
    {
        this.parkingList = apps;
        this.context = context;
    }
    public static class ViewHolder
    {
        public static FrameLayout background;
        public TextView DataText;
        public ImageView cardImage;


    }
    @Override
    public int getCount()
    {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        View rowView = convertView;


        if (rowView == null) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item, parent, false);
            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
            viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
            viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
            rowView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");
        Glide.with(context).load(parkingList.get(position).getImagePath()).into(viewHolder.cardImage);

        return rowView;
    }
}
