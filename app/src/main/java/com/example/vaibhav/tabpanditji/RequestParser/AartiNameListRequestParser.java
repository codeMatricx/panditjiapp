package com.example.vaibhav.tabpanditji.RequestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/1/2018.
 */

public class AartiNameListRequestParser
{
    @SerializedName("aartiId")
    @Expose
    public String aartiId;
}
