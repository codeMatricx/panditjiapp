package com.example.vaibhav.tabpanditji.RequestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/2/2018.
 */

public class PoojaVidhiRequestParser
{
    @SerializedName("poojaId")
    @Expose
    public String poojaId;
}
