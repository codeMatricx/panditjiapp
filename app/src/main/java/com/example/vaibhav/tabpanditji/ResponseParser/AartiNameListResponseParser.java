package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kumar on 2/1/2018.
 */

public class AartiNameListResponseParser
{
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("aartiGodName")
    @Expose
    public String aartiGodName;
    @SerializedName("aartiNameList")
    @Expose
    public List<Object> aartiNameList;
}
