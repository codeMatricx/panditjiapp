package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 2/6/2018.
 */

public class GetKatha {
    @SerializedName("kathaName")
    @Expose
    public String kathaName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("katha")
    @Expose
    public String katha;
}
