package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/3/2018.
 */

public class PoojaList {
    @SerializedName("poojaId")
    @Expose
    public String poojaId;
    @SerializedName("poojaName")
    @Expose
    public String poojaName;
}
