package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kumar on 2/5/2018.
 */

public class YogaDetails_ResponseParser
{
    @SerializedName("responseCode")
    @Expose
    public Integer responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("yogaName")
    @Expose
    public String yogaName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("stepList")
    @Expose
    public List<StepList> stepList;
}
