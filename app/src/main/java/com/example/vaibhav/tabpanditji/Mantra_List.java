package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.vaibhav.tabpanditji.Adapter.MantrasList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.Katha_Requestparser;
import com.example.vaibhav.tabpanditji.ResponseParser.KathaListResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.MantrasList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class Mantra_List extends AppCompatActivity {

    ListView listView_mantra;
    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_mantra__list);
        listView_mantra = (ListView) findViewById(R.id.mantra_list_id);
        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        new NewMantraList().execute();
    }

    private class NewMantraList extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private MantrasList_ResponseParser mMantraListResponseParser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Mantra_List.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mMantraListResponseParser = gson.fromJson(response, MantrasList_ResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mMantraListResponseParser != null) {
                if (mMantraListResponseParser.responseCode.trim().equals("200")) {
                    MantrasList_Adapter mAll_Adapter = new MantrasList_Adapter(Mantra_List.this, mMantraListResponseParser.mantraList);
                    listView_mantra.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();
                } else {
                    CommonMethod.showAlert(mMantraListResponseParser.responseMessage.toString().trim(),
                            Mantra_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Mantra_List.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.MANTRAS_LIST;
            Log.e("url----------------", "" + url);
            try {
                Katha_Requestparser mkatha_request = new Katha_Requestparser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(mkatha_request, Katha_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Mantra_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

