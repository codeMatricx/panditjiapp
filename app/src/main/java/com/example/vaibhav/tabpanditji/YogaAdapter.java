package com.example.vaibhav.tabpanditji;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by vaibhav on 3/1/18.
 */

public class YogaAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] yoga_desc;
    private final Integer[] yoga_img;

    public YogaAdapter(Activity context, Integer[] yoga_img,String[] yoga_desc) {
        super(context, R.layout.layout_yoga, yoga_desc);
        this.context = context;
        this.yoga_desc = yoga_desc;
        this.yoga_img = yoga_img;
    }


    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.layout_yoga, null,true);

        TextView desc= (TextView) rowView.findViewById(R.id.tv_yoga);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.Iv_yoga);



        desc.setText(yoga_desc[position]);
        // txttime.setText(times[position]);
        imageView.setImageResource(yoga_img[position]);
        return rowView;

    };

}
