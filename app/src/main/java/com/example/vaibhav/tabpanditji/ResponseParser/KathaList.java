package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/2/2018.
 */

public class KathaList
{
    @SerializedName("kathaId")
    @Expose
    public String kathaId;
    @SerializedName("kathaName")
    @Expose
    public String kathaName;
}
