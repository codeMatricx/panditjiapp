package com.example.vaibhav.tabpanditji.RequestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 2/6/2018.
 */

public class Poojavidi_Request {
    @SerializedName("poojaId")
    @Expose
    public String poojaId;
}
