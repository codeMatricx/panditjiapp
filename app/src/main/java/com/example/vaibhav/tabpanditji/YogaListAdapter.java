package com.example.vaibhav.tabpanditji;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class YogaListAdapter extends ArrayAdapter<YogaListClass> {
    private Context mContext;
    int mResource;

    public YogaListAdapter(@NonNull Context context, int resource,
    @NonNull List<YogaListClass> objects) {
        super (context, resource, objects);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        String mYogaName = getItem (position).getmYogaName ();

        YogaListClass yogaListClass  = new YogaListClass (mYogaName);

        LayoutInflater inflater = LayoutInflater.from (mContext);
        convertView = inflater.inflate (mResource, parent, false);

        TextView tvText = (TextView) convertView.findViewById (R.id.xYoganame);

        tvText.setText (mYogaName);
        return convertView;
    }

}