package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.KathaList;
import com.example.vaibhav.tabpanditji.ResponseParser.YogaList;

import java.util.List;

/**
 * Created by kumar on 2/3/2018.
 */

public class YogaList_Adapter extends BaseAdapter {
    private Context mContext;

    private List<YogaList> yogaList;

    public YogaList_Adapter(Context mContext, List<YogaList> yogaList)
    {
        this.mContext = mContext;
        this.yogaList = yogaList;
    }

    @Override
    public int getCount() {
        return yogaList.size();
    }

    @Override
    public Object getItem(int position) {
        return yogaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v=View.inflate(mContext, R.layout.activity_yoga__list1,null);

        TextView nameTextView=(TextView)v.findViewById(R.id.tv_yogalist_adap);
        nameTextView.setText(yogaList.get(position).yogaName);


        return v;
    }
}