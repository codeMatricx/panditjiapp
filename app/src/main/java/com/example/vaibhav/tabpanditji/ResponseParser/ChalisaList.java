package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/5/2018.
 */

public class ChalisaList
{
    @SerializedName("chalisaId")
    @Expose
    public String chalisaId;
    @SerializedName("chalisaName")
    @Expose
    public String chalisaName;
}
