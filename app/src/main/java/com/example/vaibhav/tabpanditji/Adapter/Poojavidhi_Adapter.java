package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.StepList;
import com.example.vaibhav.tabpanditji.ResponseParser.VidhiStep;

import java.util.List;

/**
 * Created by iSiwal on 2/6/2018.
 */

public class Poojavidhi_Adapter extends BaseAdapter {

    private Context mContext;

    private List<VidhiStep> vidhiStep;

    public Poojavidhi_Adapter(Context mContext, List<VidhiStep> vidhiStep  )
    {
        this.mContext = mContext;
        this.vidhiStep = vidhiStep;
    }

    @Override
    public int getCount() {
        return vidhiStep.size();
    }

    @Override
    public Object getItem(int position) {
        return vidhiStep.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Poojavidhi_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.poojavidi_adap, null);
            holder = new Poojavidhi_Adapter.Holder();
            holder.step=(TextView)convertView.findViewById(R.id.tv_vidhistep);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_vidhidesc);
            holder.img_step=(ImageView)convertView.findViewById(R.id.image_vidhi);
            convertView.setTag(holder);
        }
        else
        {
            holder = (Poojavidhi_Adapter.Holder) convertView.getTag();
        }

        holder.step.setText(vidhiStep.get(position).step);
        holder.desc.setText(vidhiStep.get(position).stepDetail);
        //holder.img_step.setImageResource(Integer.parseInt(vidhiStep.get(position).stepImage));
        return convertView;
    }

    public class Holder {
        TextView step,desc;
        ImageView img_step;



    }


}
