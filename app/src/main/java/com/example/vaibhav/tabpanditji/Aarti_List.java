package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.Adapter.AartiList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.AartiList_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import org.apache.http.entity.StringEntity;



public class Aarti_List extends AppCompatActivity
{
    private ListView listView_item;

    Toolbar mtoolbar_top;
    SearchView searchView;
    TextView et_textname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_aarti__list);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_top);
        listView_item = (ListView) findViewById(R.id.aarti_list_id);
        searchView = (SearchView) findViewById(R.id.menu_toolbarsearch);
        et_textname=(TextView)findViewById(R.id.aartiListText_id);

        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        new AartiList().execute();
    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

            MenuInflater inflater = getMenuInflater();

            inflater.inflate(R.menu.aarti_list, menu);

            MenuItem item = menu.findItem(R.id.search_menu);

            SearchView searchView = (SearchView) item.getActionView();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
            return super.onCreateOptionsMenu(menu);
        }

        //New AartiList
        private class AartiList extends AsyncTask<String, Void, String> {
            private ProgressDialog progress;
            private AartiList_ResponseParser artiList_responseParser;
            private String response = "";
            boolean valid = true;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = ProgressDialog.show(Aarti_List.this, "", "Loading...", true);
                progress.setCancelable(true);
            }

            @Override
            protected String doInBackground(String... params) {
                response = CallSignupService();
                return response;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                progress.dismiss();

                try {
                    // get response from server side and store in SignupResponse Parser///////
                    Gson gson = new Gson();
                    artiList_responseParser = gson.fromJson(response,AartiList_ResponseParser .class);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                } catch (JsonIOException e) {
                    e.printStackTrace();
                }
                if (artiList_responseParser != null) {
                    if (artiList_responseParser.responseCode.trim().equals("200")) {
                        AartiList_Adapter mAll_Adapter = new AartiList_Adapter(Aarti_List.this, artiList_responseParser.aartiList);
                        listView_item.setAdapter(mAll_Adapter);
                        mAll_Adapter.notifyDataSetChanged();
                    } else {
                        CommonMethod.showAlert(artiList_responseParser.responseMessage.toString().trim(), Aarti_List.this);
                    }
                } else {
                    CommonMethod.showAlert(getResources().getString(R.string.connection_error), Aarti_List.this);
                }
            }

            @SuppressLint("DefaultLocale")

            //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

            private String CallSignupService() {
                String url = Constant.BASE_URL + Constant.ARTI_LIST;
                Log.e("url----------------", "" + url);
                try {
                    AartiList_RequestParser artiList_requestParser = new AartiList_RequestParser();
                    Gson gson = new Gson();
                    String requestInString = gson.toJson(artiList_requestParser, AartiList_RequestParser.class);
                    System.out.println(requestInString);
                    StringEntity stringEntity = new StringEntity(requestInString);
                    ServiceCall serviceCall = new ServiceCall(Aarti_List.this, url, stringEntity);
                    response = serviceCall.getServiceResponse();
                    Log.e("Response -- ", "" + response);
                    System.out.println(serviceCall.getServiceResponse());




                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }
        }
}
