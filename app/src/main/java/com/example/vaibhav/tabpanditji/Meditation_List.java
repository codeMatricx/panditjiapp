package com.example.vaibhav.tabpanditji;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.vaibhav.tabpanditji.Adapter.MedetationList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.MedetationList_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.Medetation_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class Meditation_List extends AppCompatActivity {

    ListView meditation_list;

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_meditation__list);
        meditation_list = (ListView) findViewById(R.id.meditation_list_id);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        new NewMeditation_List().execute();
    }

    private class NewMeditation_List extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Medetation_ResponseParser medetation_responseParser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Meditation_List.this, "", "Loading.", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                medetation_responseParser = gson.fromJson(response, Medetation_ResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (medetation_responseParser != null) {
                if (medetation_responseParser.responseCode.trim().equals("200"))
                {
                    MedetationList_Adapter medetationList_adapter = new MedetationList_Adapter(Meditation_List.this, medetation_responseParser.meditationList);
                    meditation_list.setAdapter(medetationList_adapter);
                    medetationList_adapter.notifyDataSetChanged();

                } else {
                    CommonMethod.showAlert(medetation_responseParser.responseMessage.toString().trim(), Meditation_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Meditation_List.this);
            }
        }


        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.MEDITATIONLIST;
            Log.e("url----------------", "" + url);

            try {
                MedetationList_RequestParser medetationList_requestParser = new MedetationList_RequestParser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(medetationList_requestParser, MedetationList_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Meditation_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
