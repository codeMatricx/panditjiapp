package com.example.vaibhav.tabpanditji.RequestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 2/6/2018.
 */

public class Katha_Request {
    @SerializedName("kathaId")
    @Expose
    public String kathaId;
}
