package com.example.vaibhav.tabpanditji;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Ravivar_Vrat_KathaActivity extends AppCompatActivity {

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_ravivar__vrat__katha);

        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);

        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
