package com.example.vaibhav.tabpanditji;

public class YogaListClass {
   private String mYogaName;

    public YogaListClass(String mYogaName) {
        this.mYogaName = mYogaName;
    }

    public String getmYogaName() {
        return mYogaName;
    }

    public void setmYogaNAme(String mYogaName) {
        this.mYogaName = mYogaName;
    }
}