package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/2/2018.
 */

public class StepList
{
    @SerializedName("step")
    @Expose
    public String step;
    @SerializedName("stepDetail")
    @Expose
    public String stepDetail;
    @SerializedName("stepImage")
    @Expose
    public String stepImage;
}
