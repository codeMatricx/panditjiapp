package com.example.vaibhav.tabpanditji.YogaListResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YogaListResponse {
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("yogaList")
    @Expose
    public List<YogaList> yogaList;
}