package com.example.vaibhav.tabpanditji.YogaListService.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.vaibhav.tabpanditji.R;


/**
 * Created by iSiwal on 11/30/2017.
 */

public class AlertDialogManager {
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle("Logout");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure want to Logout ?");

        if(status != null)
            // Setting alert dialog icon
            alertDialog.setIcon((status) ? R.drawable.sucess : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
