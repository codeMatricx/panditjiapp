package com.example.vaibhav.tabpanditji.YogaListService.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {

    private static Editor editor;
    public static String NOTIFICATION_STATUS="NOTIFICATION_STATUS";

    /**
     * Called for setting the value based on key in Prefs
     */

    //--------------------------------------------------
    public static void saveUseridemergency(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("E_userid", value);
        editor.commit();

    }
    public static String getsaveUseridemergency(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Euserid = sharedPreferences.getString("E_userid", "");
        return Euserid;

    }
    //--------------------------------------------------
    public static void saveContactidemergency(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Contact", value);
        editor.commit();

    }
    public static String getsaveContactidemergency(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Econtact = sharedPreferences.getString("Contact", "");
        return Econtact;

    }
    //--------------------------------------------------
    public static void saveLaterBookingid(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("LaterBooking", value);
        editor.commit();

    }
    public static String getsaveLaterBookingid(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String BOOKINGID = sharedPreferences.getString("LaterBooking", "");
        return BOOKINGID;

    }
//--------------------------------------------------
public static void saveDeviceToken(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("Device_token", value);
    editor.commit();

}
    public static String getSavedDeviceToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DEVICETOKEN = sharedPreferences.getString("Device_token", "");
        return DEVICETOKEN;

    }

    //==============================================================
public static void saveBookingID(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("Booking_ID", value);
    editor.commit();

}
    public static String getSavedPreferencesBookingID(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String BOOKINGID = sharedPreferences.getString("Booking_ID", "");
        return BOOKINGID;

    }
//===========================================================================
public static void savePassengerID(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("Pasengr_ID", value);
    editor.commit();

}
    public static String getSavedPreferencesPassengerID(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String PASSENGERID = sharedPreferences.getString("Pasengr_ID", "");
        return PASSENGERID;

    }
//===========================================================

    public static void saveFinalBill(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("FinalBill", value);
        editor.commit();

    }
    public static String getSavedPreferencesFinalBill(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Final_Bill = sharedPreferences.getString("FinalBill", "");
        return Final_Bill;

    }

//====================================================================

    public static void savePassword(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Driver_Password", value);
        editor.commit();

    }
    public static String getSavedPreferencesPassword(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DRIVERID = sharedPreferences.getString("Driver_Password", "");
        return DRIVERID;

    }


//===========================================================


    public static void saveLoginPreferencesUserId(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("USERID", value);
        editor.commit();
    }


    public static void saveEmailid(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("EMAILID", value);
        editor.commit();

    }
    public static String getSavedPreferencesEmailid(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_EMAILID_ = sharedPreferences.getString("EMAILID", "");
        return login_EMAILID_;

    }
 //-------------------------------------------------------
    public static void savePhone(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PHONE", value);
        editor.commit();

    }
    public static String getSavedPreferencesPhone(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_PHONE = sharedPreferences.getString("PHONE", "");
        return login_PHONE;

    }
//-----------------------------------------------

    public static void saveCabname(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("CABNAME", value);
        editor.commit();

    }
    public static String getSavedPreferencesCABname(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String cab_name = sharedPreferences.getString("CABNAME", "");
        return cab_name;

    }

 //--------------------------------------------------
 public static void saveRide_source(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("R_SOURCE", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_source(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_source = sharedPreferences.getString("R_SOURCE", "");
        return ride_source;

    }
 //------------------------------------------------------
 public static void saveRide_destination(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("RIDE_DESTINATION", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_destination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_desti = sharedPreferences.getString("RIDE_DESTINATION", "");
        return ride_desti;

    }
 //------------------------------------------------------
 public static void saveRide_fare(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("RIDE_FARE", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_fare(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_fare = sharedPreferences.getString("RIDE_FARE", "");
        return ride_fare;

    }
 //--------------------------------------------------------------------

    public static void saveRide_Source(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_SOURCE", value);
        editor.commit();

    }
    public static String getSavedPreferencesRide_Source(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_Sources = sharedPreferences.getString("RIDE_SOURCE", "");
        return ride_Sources;

    }
//---------------------------------------
public static void saveSrcLatlang(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("SRCLATLANG", value);
    editor.commit();

}
    public static String getSavedSrcLatlang(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String srclatlng = sharedPreferences.getString("SRCLATLANG", "");
        return srclatlng;

    }
//----------------------------------------------------------

    public static void saveRide_Destinations(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_DESTINATIONS", value);
        editor.commit();

    }
    public static String getSavedPreferencesRide_Destinations(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_Destinationses = sharedPreferences.getString("RIDE_DESTINATIONS", "");
        return ride_Destinationses;

    }
//=======================================================================

    public static void saveRidetype(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_TYPE", value);
        editor.commit();

    }
    public static String getSavedPreferencesRidetype(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_TYPE = sharedPreferences.getString("RIDE_TYPE", "");
        return ride_TYPE;

    }

    //------------------------------------------------
public static void saveRideSource(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("RIDE_SOURCE", value);
    editor.commit();

}
    public static String getSavedPreferencesRideSource(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_source = sharedPreferences.getString("RIDE_SOURCE", "");
        return ride_source;

    }

//--------------------------------------------------------------------

    public static void saveRideDestination(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_DESTINATIONSES", value);
        editor.commit();

    }
    public static String getSavedPreferencesRideDestination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_destinationes = sharedPreferences.getString("RIDE_DESTINATIONSES", "");
        return ride_destinationes;

    }



//-------------------------------------------------------
public static void saveRide_Time(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("RIDE_TIME", value);
    editor.commit();

}
    public static String getSavedPreferencesRide_Time(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_times = sharedPreferences.getString("RIDE_TIME", "");
        return ride_times;

    }

//---------------------------------------------------
    public static void saveUserName(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Username", value);
        editor.commit();

    }
    public static String getSavedPreferencesUserName(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_USERID = sharedPreferences.getString("Username", "");
        return login_USERID;

    }
//=========================================================
    public static void saveDriverName(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Drivername", value);
        editor.commit();
    }

    public static String getSavedPreferencesDrivername(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DRIVERNAME = sharedPreferences.getString("Drivername", "");
        return DRIVERNAME;
    }
//==============================================================
public static void saveDriverNumber(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("Drivernumber", value);
    editor.commit();
}

    public static String getSavedPreferencesDriverNumber(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DRIVERNUMBER = sharedPreferences.getString("Drivernumber", "");
        return DRIVERNUMBER;
    }
 //=================================================================
 public static void saveVehicleName(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("Vehiclename", value);
     editor.commit();
 }

    public static String getSavedPreferencesVehicleName(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String VEHICLENAME = sharedPreferences.getString("Vehiclename", "");
        return VEHICLENAME;
    }
  //==============================================================
  public static void saveVehicleNumber(Context context, String value)
  {
      SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
      Editor editor = sharedPreferences.edit();
      editor.putString("Vehiclenumber", value);
      editor.commit();
  }

    public static String getSavedPreferencesVehicleNumber(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String VEHICLENUMBER = sharedPreferences.getString("Vehiclenumber", "");
        return VEHICLENUMBER;
    }
 //===============================================================
    public static String getDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date value = null;
        try {
            value = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mmaa");
        dateFormatter.setTimeZone(TimeZone.getDefault());
        String dt = dateFormatter.format(value);

        return dt;
    }


    /**
     * Called for getting the value based on key from Prefs
     */
    public static String getPrefsData(Context context, String prefsKey,
                                      String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String prefsValue = sharedPreferences.getString(prefsKey, defaultValue);
        return prefsValue;
    }

    /**
     * Called for Showing Alert in Application
     */
    public static void showAlert(String message, Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Called for checking Internet connection
     */
    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo;
        try {
            netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }


    // for storing the latitude
    public static void saveLattitudeSource(Context applicationContext, double latitude) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        Editor editor = sharedPreferences.edit();
        editor.putString("Latsource", String.valueOf(latitude));
        editor.commit();
    }
    public static String getLattitudeSource(Context applicationContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        String ride_times = sharedPreferences.getString("Latsource", "");
        return ride_times;
    }
//--------------------------------------
public static void saveLongitudeSource(Context applicationContext, double longitude) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
    Editor editor = sharedPreferences.edit();
    editor.putString("Longsource", String.valueOf(longitude));
    editor.commit();
}
    public static String getLongitudeSource(Context applicationContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        String ride_times = sharedPreferences.getString("Longsource", "");
        return ride_times;
    }


//==========================================================================
}