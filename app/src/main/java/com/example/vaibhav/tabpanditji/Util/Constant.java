package com.example.vaibhav.tabpanditji.Util;

public class Constant {

	public static final String FALSE = "false";
	public static final String TXT_BLANK = "";

	// cama
	public static String UTF_8 = "UTF-8";
	public static String SERVER_RESPONSE_SUCCESS_CODE = "200";
	public static String CONTENT_TYPE = "Content-type";
	public static String APPLICATION_JSON = "application/json";
	public static String ACCESS = "Access";
	public static final String RESPONCE_CODE = "responseCode";
	public static final String RESPONCE_MESSAGE = "responseMessage";

	public static final String RESPONCECODE = "responsecode";
	public static final String RESPONCEMESSAGE = "responsemessage";

	/***  App Preference Keys */
	public static final String PREF_USERNAME 			= "fullname";
	public static final String PREF_USERID 				= "USER_ID";
	public static final String PREF_STSFF_USERID 		= "USER_ID";
	public static final String PREF_USER_IMAGE 			= "image";

	public static final String BASE_URL = "http://staging.isiwal.com/panditji/panditjiapi/";

	public static final String MANTRAS_LIST = "getMantrasList.php";
	public static final String POOJA_LIST = "getPoojaList.php";
	public static final String KATHA_LIST = "getKathaList.php";
	public static final String YOGA_LIST = "getYogaList.php";
	public static final String ARTI_LIST = "getAartiList.php";
	public static final String HOME_REMEDIS = "getHomeRemediesList.php";
	public static final String MEDITATIONLIST = "getMeditationList.php";
	public static final String CHALISALIST = "getChalisaList.php";
	public static final String GetYogaDetails = "getYogaDetails.php";
	public static final String GetArtiNameList = "getAartiNameList.php";
	public static final String GetPoojaVidhi = "getPoojaVidhi.php";
	public static final String GetKathaDesc ="getKatha.php";



}
