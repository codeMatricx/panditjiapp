package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kumar on 2/2/2018.
 */

public class PoojaVidhiResponseParser
{
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("poojaName")
    @Expose
    public String poojaName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("stepList")
    @Expose
    public List<StepList> stepList;
}
