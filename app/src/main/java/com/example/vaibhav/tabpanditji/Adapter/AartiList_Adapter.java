package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList;

import java.util.List;

/**
 * Created by kumar on 2/3/2018.
 */

public class AartiList_Adapter extends BaseAdapter
{
    private Context mContext;

    private List<AartiList> aartiList;;

    public AartiList_Adapter(Context mContext, List<AartiList> aartiList)
    {
        this.mContext = mContext;
        this.aartiList = aartiList;
    }

    @Override
    public int getCount() {
        return aartiList.size();
    }

    @Override
    public Object getItem(int position) {
        return aartiList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        AartiList_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_aarti__list1, null);
            holder = new AartiList_Adapter.Holder();
            holder.name=(TextView)convertView.findViewById(R.id.aartiListText_id);


            convertView.setTag(holder);
        }
        else
        {
            holder = (AartiList_Adapter.Holder) convertView.getTag();
        }

         holder.name.setText(aartiList.get(position).godNameList);

        return convertView;

    }

    public class Holder {
        TextView name;



    }


}
