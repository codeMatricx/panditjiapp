package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 2/6/2018.
 */

public class Poojavidhi_Response {
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("poojaName")
    @Expose
    public String poojaName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("vidhiStep")
    @Expose
    public List<VidhiStep> vidhiStep ;
}
