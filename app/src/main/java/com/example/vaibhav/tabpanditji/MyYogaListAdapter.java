package com.example.vaibhav.tabpanditji;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vaibhav.tabpanditji.YogaListResponseParser.YogaList;
import com.google.gson.Gson;


import java.util.List;
/**
 * Created by iSiwal on 1/27/2018.
 */
public class MyYogaListAdapter extends BaseAdapter {
    Context context;
    TextView name;
    List<YogaList> list;

    public MyYogaListAdapter(Context context, List<YogaList> list)
    {
        this.context = context;
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MyYogaListAdapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.yogalist_view, null);
            holder.name=(TextView)convertView.findViewById(R.id.xYoganame);
            convertView.setTag(holder);
        }
        else
        {
            holder = (MyYogaListAdapter.Holder) convertView.getTag();
        }
        holder.name.setText(list.get(position).yogaName);
        return convertView;

    }

    public class Holder {
        TextView name;
        ImageView images_delete;


    }



}
