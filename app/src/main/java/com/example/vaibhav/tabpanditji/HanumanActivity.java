package com.example.vaibhav.tabpanditji;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class HanumanActivity extends AppCompatActivity {

    ImageButton btnPlay,btnStop,btnPause;

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hanuman);


        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);

        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        //id of play and stop buttons
        btnPlay=(ImageButton)findViewById(R.id.btn_play);
        btnStop=(ImageButton) findViewById(R.id.btn_stop);
        btnPause=(ImageButton) findViewById(R.id.btn_pause);

        //click listener for play music button
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Play Music", Toast.LENGTH_SHORT).show();
            }
        });

        //click listeners for pause music
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Pause Music", Toast.LENGTH_SHORT).show();
            }
        });

        //click listeners for Stop music
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Stop Music", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
