package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.HomeRemediesList;

import java.util.List;

/**
 * Created by iSiwal on 2/3/2018.
 */

public class HomeRemedies_Adapter extends BaseAdapter {
    Context context;
    TextView name;
    List<HomeRemediesList> homeRemediesList;
    public HomeRemedies_Adapter(Context context, List<HomeRemediesList> homeRemediesList)
    {
        this.context = context;
        this.homeRemediesList=homeRemediesList;
    }

    @Override
    public int getCount() {
        return homeRemediesList.size();
    }
    @Override
    public Object getItem(int position) {
        return homeRemediesList.get(position);
    }
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        HomeRemedies_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.homeremedieslist_layout, null);
            holder.name=(TextView)convertView.findViewById(R.id.tv_homeremedieslist_adap);
            convertView.setTag(holder);
        }
        else
        {
            holder = (HomeRemedies_Adapter.Holder) convertView.getTag();
        }
        holder.name.setText(homeRemediesList.get(position).homeRemediesName);
        return convertView;

    }

    public class Holder {
        TextView name;

    }

}

