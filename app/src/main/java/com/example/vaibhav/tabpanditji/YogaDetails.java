package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.Adapter.AartiList_Adapter;
import com.example.vaibhav.tabpanditji.Adapter.YogaDetails_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.AartiList_RequestParser;
import com.example.vaibhav.tabpanditji.RequestParser.YogaDetails_Request;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList_ResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.YogaDetails_Response;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class YogaDetails extends AppCompatActivity {
    ListView listView;
    TextView tv_step,tv_desc;
    ImageView img_step;
    Toolbar mtoolbar_top;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yoga_details);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_step=(TextView)findViewById(R.id.tv_yoganame);
        String name=CommonMethod.getsavyogalistname(YogaDetails.this);
        tv_step.setText(name);
        listView=(ListView)findViewById(R.id.list_yogadetails);

        new Yogadetail().execute();
    }

    private class Yogadetail extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private YogaDetails_Response yogaDetails_response;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(YogaDetails.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                yogaDetails_response = gson.fromJson(response,YogaDetails_Response .class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (yogaDetails_response != null) {
                if (yogaDetails_response.responseCode.trim().equals("200")) {
                    YogaDetails_Adapter yogaDetails_adapter=new YogaDetails_Adapter(YogaDetails.this,yogaDetails_response.stepList);
                    listView.setAdapter(yogaDetails_adapter);
                    yogaDetails_adapter.notifyDataSetChanged();
                } else {
                    CommonMethod.showAlert(yogaDetails_response.responseMessage.toString().trim(), YogaDetails.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), YogaDetails.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.GetYogaDetails;
            Log.e("url----------------", "" + url);
            try {
                String abc=CommonMethod.getsaveUseridyogalist(YogaDetails.this);
                YogaDetails_Request yogaDetails_request = new YogaDetails_Request();
                yogaDetails_request.yogaId=abc;
                Log.e("YogalistIdes",""+abc);
                Gson gson = new Gson();
                String requestInString = gson.toJson(yogaDetails_request, YogaDetails_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(YogaDetails.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

}

