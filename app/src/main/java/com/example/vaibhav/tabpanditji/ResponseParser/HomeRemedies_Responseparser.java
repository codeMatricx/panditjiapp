package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 2/3/2018.
 */

public class HomeRemedies_Responseparser {
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("homeRemediesList")
    @Expose
    public List<HomeRemediesList> homeRemediesList ;
}
