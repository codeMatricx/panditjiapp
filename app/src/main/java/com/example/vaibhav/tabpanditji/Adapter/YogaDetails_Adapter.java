package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.StepList;

import java.util.List;

/**
 * Created by iSiwal on 2/5/2018.
 */

public class YogaDetails_Adapter extends BaseAdapter {

    private Context mContext;

    private List<StepList> stepList;

    public YogaDetails_Adapter(Context mContext, List<StepList> stepList )
    {
        this.mContext = mContext;
        this.stepList = stepList;
    }

    @Override
    public int getCount() {
        return stepList.size();
    }

    @Override
    public Object getItem(int position) {
        return stepList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        YogaDetails_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_balasana, null);
            holder = new YogaDetails_Adapter.Holder();
            holder.step=(TextView)convertView.findViewById(R.id.tv_step);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_desc);
            holder.img_step=(ImageView)convertView.findViewById(R.id.balasana_image);
            convertView.setTag(holder);
        }
        else
        {
            holder = (YogaDetails_Adapter.Holder) convertView.getTag();
        }

        holder.step.setText(stepList.get(position).step);
        holder.desc.setText(stepList.get(position).stepDetail);
        holder.img_step.setImageResource(Integer.parseInt(stepList.get(position).stepImage));
        return convertView;
    }

    public class Holder {
        TextView step,desc;
        ImageView img_step;



    }




}
