package com.example.vaibhav.tabpanditji;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

public class MainNavigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    ArrayList<Data> array;
    RelativeLayout.LayoutParams layoutparams;
    SwipeFlingAdapterView flingContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_navigation);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Defining ViewPager
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Puja Samagri"));
        tabLayout.addTab(tabLayout.newTab().setText("Pandit Booking"));
        tabLayout.addTab(tabLayout.newTab().setText("Ayurvedic Medicine"));
        tabLayout.addTab(tabLayout.newTab().setText("Patanjali Products"));
        tabLayout.addTab(tabLayout.newTab().setText("Ayurvedic Medicine"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

        // Adding  DrawerLayout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        // NavigationView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    // --------------------OnCreate Method Finish----------------------------

    // Adding the Fragments and tabs name
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OneFragment(), "Book Panditji");
        adapter.addFrag(new TwoFragment(), "Puja Samagri");
        adapter.addFrag(new ThreeFragment(), "Patanjali Product");
        adapter.addFrag(new FourFragment(), "Ayurveda Medicine");
        adapter.addFrag(new FiveFragment(),"Statue");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_toolbarsearch)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_arti) {
            // Handle the camera action
            Intent intent=new Intent(MainNavigation.this,Aarti_List.class);
            startActivity(intent);
        } else if (id == R.id.nav_chalisa) {
            Intent intent=new Intent(MainNavigation.this,Chalisa_List.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_mantra) {
            Intent intent=new Intent(MainNavigation.this,Mantra_List.class);
            startActivity(intent);

        } else if (id == R.id.nav_katha) {
            Intent intent=new Intent(MainNavigation.this,Katha_List.class);
            startActivity(intent);

        } else if (id == R.id.nav_poojavidhi) {
            Intent intent=new Intent(MainNavigation.this,Pooja_List.class);
            startActivity(intent);

        }else if (id == R.id.nav_meditation) {
            Intent intent=new Intent(MainNavigation.this,Meditation_List.class);
            startActivity(intent);

        }else if (id == R.id.nav_yoga) {
            Intent intent=new Intent(MainNavigation.this,Yoga_List.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_home_remedies){
            Intent intent=new Intent(MainNavigation.this,HomeRemedies.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
