package com.example.vaibhav.tabpanditji;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.ResponseParser.KathaList;

import java.util.List;

public class KathaListAdapter extends BaseAdapter
{
    private Context mContext;

    private List<KathaList> mKAthaListItem;

    public KathaListAdapter(Context mContext, List<KathaList> mKAthaListItem)
    {
        this.mContext = mContext;
        this.mKAthaListItem = mKAthaListItem;
    }

    @Override
    public int getCount() {
        return mKAthaListItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mKAthaListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v=View.inflate(mContext,R.layout.activity_katha__list1,null);

        TextView nameTextView=(TextView)v.findViewById(R.id.tv_kathaList_id);
        nameTextView.setText(mKAthaListItem.get(position).kathaName);

        return v;
    }
}
