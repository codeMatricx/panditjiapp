package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vaibhav.tabpanditji.RequestParser.Katha_Requestparser;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiNameListResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.KathaListResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;


public class Katha_List extends AppCompatActivity {

    ListView katha_list;
    //String[] katha_list_values={"Ravivar Vrat Katha","Somvar Vrat Katha","Mangalbar Vrat Katha","Budhbar Vrat Katha","Brashpatibar Vrat Katha","Sukrabar Vrat Katha","Sanivar Vrat Katha"};
    Toolbar mtoolbar_top;

    TextView et_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_katha__list);
        et_name=(TextView)findViewById(R.id.tv_kathaList_id);
        katha_list=(ListView)findViewById(R.id.katha_list_id);
        mtoolbar_top =(Toolbar)findViewById (R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        new NewKathaList().execute();

    }

    private class NewKathaList extends AsyncTask<String, Void, String>
    {
        private ProgressDialog progress;
        private KathaListResponseParser mkatharesponse;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Katha_List.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mkatharesponse = gson.fromJson(response, KathaListResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mkatharesponse != null) {
                if (mkatharesponse.responseCode.trim().equals("200")) {
                    KathaListAdapter mAll_Adapter = new KathaListAdapter(Katha_List.this, mkatharesponse.kathaList);
                    katha_list.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                    katha_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String item = ((TextView)view).getText().toString();
                            CommonMethod.savekathaname(getApplicationContext(),item);
                          String ides=  mkatharesponse.kathaList.get(position).kathaId;
                            Intent i = new Intent(Katha_List.this,KathaDetails.class);
                            startActivity(i);
                            CommonMethod.savekathaid(getApplicationContext(),ides);
                        }
                    });
                } else {
                    CommonMethod.showAlert(mkatharesponse.responseMessage.toString().trim(),
                            Katha_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Katha_List.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.KATHA_LIST;
            Log.e("url----------------", "" + url);
            try {
                Katha_Requestparser mkatha_request = new Katha_Requestparser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(mkatha_request, Katha_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Katha_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}