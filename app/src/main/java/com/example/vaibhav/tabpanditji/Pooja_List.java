package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vaibhav.tabpanditji.Adapter.PoojaList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.Katha_Requestparser;
import com.example.vaibhav.tabpanditji.RequestParser.PoojaList_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.KathaListResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.PoojaList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class Pooja_List extends AppCompatActivity {

    ListView pooja_list;

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_pooja__list);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_poojalist);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        pooja_list = (ListView) findViewById(R.id.pooja_list_id);


        new NewPoojaList().execute();
    }
    private class NewPoojaList extends AsyncTask<String, Void, String>
    {
        private ProgressDialog progress;
        private PoojaList_ResponseParser mPoojaListResponseParser;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Pooja_List.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mPoojaListResponseParser = gson.fromJson(response, PoojaList_ResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mPoojaListResponseParser != null) {
                if (mPoojaListResponseParser.responseCode.trim().equals("200")) {
                    PoojaList_Adapter mAll_Adapter = new PoojaList_Adapter(Pooja_List.this, mPoojaListResponseParser.poojaList);
                    pooja_list.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();
                    pooja_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> bookingsListView, View view, int position, long id) {
                            String item = ((TextView)view).getText().toString();
                            Toast.makeText(getApplicationContext()," "+item, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Pooja_List.this,PoojaVidhiActivity.class);
                            startActivity(i);
                            CommonMethod.savepoojaname(getApplicationContext(), item);
                            String ides =mPoojaListResponseParser.poojaList.get(position).poojaId;
                            Log.e("Yoga_ides",""+ides);
                            CommonMethod.savePoojavidhi(getApplicationContext(), ides);

                        }
                    });
                } else {
                    CommonMethod.showAlert(mPoojaListResponseParser.responseMessage.toString().trim(), Pooja_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Pooja_List.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.POOJA_LIST;
            Log.e("url----------------", "" + url);
            try {
                PoojaList_RequestParser mkatha_request = new PoojaList_RequestParser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(mkatha_request, PoojaList_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Pooja_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

