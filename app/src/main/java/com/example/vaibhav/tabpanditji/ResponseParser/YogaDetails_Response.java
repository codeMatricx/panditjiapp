package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 2/5/2018.
 */

public class YogaDetails_Response {
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("yogaName")
    @Expose
    public String yogaName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("stepList")
    @Expose
    public List<StepList> stepList ;
}
