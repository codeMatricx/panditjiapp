package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kumar on 2/3/2018.
 */

public class PoojaList_ResponseParser {
    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMessage")
    @Expose
    public String responseMessage;
    @SerializedName("poojaList")
    @Expose
    public List<PoojaList> poojaList;
}
