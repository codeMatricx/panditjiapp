package com.example.vaibhav.tabpanditji;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class Ganesh_Aarti_List extends AppCompatActivity {

    ListView ganesh_arti_list_view;

    String[] ganesh_arti_list_values={"Jai Ganesh Jai Ganesh","jai Deva Jai Deva","Ganpati ki Seva Mangal meva","Arti ki jai Gajanand ki","jai ho ganpati Jai ho ganpaty"};

    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar= getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_ganesh__aarti__list);

        ganesh_arti_list_view=(ListView)findViewById(R.id.ganesha_aarti_list_id);

        mtoolbar_top=(Toolbar)findViewById(R.id.toolbar_top);

        ArrayAdapter<String> ganehaAdapter=new ArrayAdapter<String>(this,R.layout.activity_ganesh__aarti__list1,ganesh_arti_list_values);

        ganesh_arti_list_view.setAdapter(ganehaAdapter);

        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ganesh_arti_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                }
            }
        });
    }
}
