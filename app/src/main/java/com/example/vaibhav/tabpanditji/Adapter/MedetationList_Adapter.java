package com.example.vaibhav.tabpanditji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.ResponseParser.MeditationList;

import java.util.List;

/**
 * Created by iSiwal on 2/3/2018.
 */

public class MedetationList_Adapter extends BaseAdapter {
    Context context;
    List<MeditationList> meditationList;
    public MedetationList_Adapter(Context context, List<MeditationList> meditationList)
    {
        this.context = context;
        this.meditationList=meditationList;
    }

    @Override
    public int getCount() {
        return meditationList.size();
    }
    @Override
    public Object getItem(int position) {
        return meditationList.get(position);
    }
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MedetationList_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_meditation__list1, null);
            holder = new MedetationList_Adapter.Holder();
            holder.name=(TextView)convertView.findViewById(R.id.meditationlist);

        }
        else
        {
            holder = (MedetationList_Adapter.Holder) convertView.getTag();
        }
        holder.name.setText(meditationList.get(position).meditationName);
        return convertView;
    }
    public class Holder {
        TextView name;
    }
}
