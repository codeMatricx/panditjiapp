package com.example.vaibhav.tabpanditji;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vaibhav.tabpanditji.Adapter.AartiList_Adapter;
import com.example.vaibhav.tabpanditji.Adapter.YogaDetails_Adapter;
import com.example.vaibhav.tabpanditji.Adapter.YogaList_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.Katha_Requestparser;
import com.example.vaibhav.tabpanditji.RequestParser.YogaDetails_Request;
import com.example.vaibhav.tabpanditji.RequestParser.YogaList_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.AartiList_ResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.KathaListResponseParser;
import com.example.vaibhav.tabpanditji.ResponseParser.YogaDetails_Response;
import com.example.vaibhav.tabpanditji.ResponseParser.YogaList;
import com.example.vaibhav.tabpanditji.ResponseParser.YogaList_ResponseParser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class Yoga_List extends AppCompatActivity {

    ListView yoga_listView_item;
    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar=getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_yoga__list);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        yoga_listView_item = (ListView) findViewById(R.id.yoga_list_id);

        new Yogalist().execute();
    }

    private class Yogalist extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private YogaList_ResponseParser yogaList_responseParser;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Yoga_List.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                yogaList_responseParser = gson.fromJson(response, YogaList_ResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (yogaList_responseParser != null) {
                if (yogaList_responseParser.responseCode.trim().equals("200")) {
                    YogaList_Adapter mAll_Adapter = new YogaList_Adapter(Yoga_List.this, yogaList_responseParser.yogaList);
                    yoga_listView_item.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                    yoga_listView_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> bookingsListView, View view, int position, long id) {
                            String item = ((TextView)view).getText().toString();
                            Toast.makeText(getApplicationContext()," "+item, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Yoga_List.this,YogaDetails.class);
                            startActivity(i);
                            CommonMethod.savyogalistname(getApplicationContext(), item);
                            String ides =yogaList_responseParser.yogaList.get(position).yogaId;
                            Log.e("Yoga_ides",""+ides);
                            CommonMethod.saveyogalistides(getApplicationContext(), ides);

                        }
                    });
                } else {
                    CommonMethod.showAlert(yogaList_responseParser.responseMessage.toString().trim(), Yoga_List.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Yoga_List.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.YOGA_LIST;
            Log.e("url----------------", "" + url);
            try {
                YogaList_RequestParser yogaList_requestParser = new YogaList_RequestParser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(yogaList_requestParser, YogaList_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Yoga_List.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

}
