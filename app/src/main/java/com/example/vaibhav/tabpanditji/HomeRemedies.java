package com.example.vaibhav.tabpanditji;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.vaibhav.tabpanditji.Adapter.HomeRemedies_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.HomeRemedies_RequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.HomeRemedies_Responseparser;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class HomeRemedies extends AppCompatActivity {
    ListView homeremedies_listview;
    Toolbar mtoolbar_top;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        android.support.v7.app.ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_home_remedies);

        homeremedies_listview=(ListView)findViewById(R.id.meditation_list_id);
        mtoolbar_top=(Toolbar)findViewById(R.id.toolbar_homeremedies);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //Implementing API
        new Homeremedies().execute();

    }

    private class Homeremedies extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private HomeRemedies_Responseparser homeRemedies_responseparser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(HomeRemedies.this, "", "Loading.", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                homeRemedies_responseparser = gson.fromJson(response, HomeRemedies_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (homeRemedies_responseparser != null) {
                if (homeRemedies_responseparser.responseCode.trim().equals("200"))
                {
                    HomeRemedies_Adapter homeRemedies_adapter = new HomeRemedies_Adapter(HomeRemedies.this, homeRemedies_responseparser.homeRemediesList);
                    homeremedies_listview.setAdapter(homeRemedies_adapter);
                    homeRemedies_adapter.notifyDataSetChanged();

                }
                else
                {
                    CommonMethod.showAlert(homeRemedies_responseparser.responseMessage.toString().trim(), HomeRemedies.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), HomeRemedies.this);
            }
        }

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService()
        {
            String url = Constant.BASE_URL + Constant.HOME_REMEDIS;
            Log.e("url----------------", "" + url);

            try {
                HomeRemedies_RequestParser homeRemedies_requestParser = new HomeRemedies_RequestParser();
                Gson gson = new Gson();
                String requestInString = gson.toJson(homeRemedies_requestParser, HomeRemedies_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(HomeRemedies.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return response;
        }
    }
}
