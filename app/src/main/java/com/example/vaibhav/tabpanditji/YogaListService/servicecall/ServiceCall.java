package com.example.vaibhav.tabpanditji.YogaListService.servicecall;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.example.vaibhav.tabpanditji.R;
import com.example.vaibhav.tabpanditji.YogaListService.utils.Constant;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;





public class ServiceCall {
	public static final int CONNECTION_TIME_OUT = 500000;
	public static final int SOCKET_TIME_OUT = 800000;
	private HttpPost post;
	private StringEntity stringEntity;
	public static int status_code = 0;
	private Context mContext;
	public ServiceCall(Context context, String url, StringEntity stringEntity) {
		post = new HttpPost(url);
		this.stringEntity = stringEntity;
		this.mContext=context;
	}

	public String getServiceResponse() {

		String responseText = "";
		final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		HttpConnectionParams.setConnectionTimeout(client.getParams(),CONNECTION_TIME_OUT); // Timeout Limit
		HttpConnectionParams.setSoTimeout(client.getParams(), SOCKET_TIME_OUT);
		HttpResponse response;
		StringBuffer stringBuffer = new StringBuffer("");

		try {
			post.setHeader(Constant.CONTENT_TYPE, Constant.APPLICATION_JSON);
			post.setHeader(Constant.ACCESS, Constant.APPLICATION_JSON);
			post.setEntity(stringEntity);
			response = client.execute(post);
			status_code = response.getStatusLine().getStatusCode();
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), Constant.UTF_8));

			String line = null;
			while ((line = reader.readLine()) != null) {

				stringBuffer.append(line);
			}

			
			
			responseText = stringBuffer.toString();  
			Log.e("Response", responseText);
		} catch (ConnectTimeoutException e) {
			
			//responseText = mContext.getString(R.string.connection_timeout);

		}catch (SocketTimeoutException e) {
			
		//	responseText = mContext.getString(R.string.connection_timeout);
			

		}catch (IOException e) {
			
			responseText = null;

		}
		 finally {
				if (client != null) {
					client.close();
				}
		 }
		return responseText;
	}

	

}