package com.example.vaibhav.tabpanditji.ResponseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kumar on 2/3/2018.
 */

public class MantraList {
    @SerializedName("mantraId")
    @Expose
    public String mantraId;
    @SerializedName("mantraListName")
    @Expose
    public String mantraListName;
}
