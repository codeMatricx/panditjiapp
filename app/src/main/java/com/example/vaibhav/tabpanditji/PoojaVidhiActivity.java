package com.example.vaibhav.tabpanditji;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vaibhav.tabpanditji.Adapter.Poojavidhi_Adapter;
import com.example.vaibhav.tabpanditji.RequestParser.PoojaVidhiRequestParser;
import com.example.vaibhav.tabpanditji.ResponseParser.Poojavidhi_Response;
import com.example.vaibhav.tabpanditji.Service_Call.ServiceCall;
import com.example.vaibhav.tabpanditji.Util.CommonMethod;
import com.example.vaibhav.tabpanditji.Util.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.StringEntity;

public class PoojaVidhiActivity extends AppCompatActivity {

    Toolbar mtoolbar_top;
    ListView listView;
    TextView tv_listname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
        setContentView(R.layout.activity_pooja_vidhi);
        mtoolbar_top = (Toolbar) findViewById(R.id.toolbar_top);
        mtoolbar_top.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mtoolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        tv_listname=(TextView)findViewById(R.id.tv_poojaname);
        String name=CommonMethod.getsavepoojaname(PoojaVidhiActivity.this);
        tv_listname.setText(name);
       listView=(ListView)findViewById(R.id.list_poojavidhi);
        new PoojaVidhi().execute();
    }

    private class PoojaVidhi extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Poojavidhi_Response poojaVidhiResponseParser;
        private String response = "";
        boolean valid = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(PoojaVidhiActivity.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                poojaVidhiResponseParser = gson.fromJson(response,Poojavidhi_Response .class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (poojaVidhiResponseParser != null) {
                if (poojaVidhiResponseParser.responseCode.trim().equals("200")) {
                    Poojavidhi_Adapter poojavidhi_adapter=new Poojavidhi_Adapter(PoojaVidhiActivity.this,poojaVidhiResponseParser.vidhiStep);
                    listView.setAdapter(poojavidhi_adapter);
                    poojavidhi_adapter.notifyDataSetChanged();
                } else {
                    CommonMethod.showAlert(poojaVidhiResponseParser.responseMessage.toString().trim(), PoojaVidhiActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), PoojaVidhiActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.GetPoojaVidhi;
            Log.e("url----------------", "" + url);
            try {
                String abc=CommonMethod.getsavePoojavidhi(PoojaVidhiActivity.this);
                PoojaVidhiRequestParser poojaVidhiActivity_request = new PoojaVidhiRequestParser();
                poojaVidhiActivity_request.poojaId=abc;
                Log.e("YogalistIdes",""+abc);
                Gson gson = new Gson();
                String requestInString = gson.toJson(poojaVidhiActivity_request, PoojaVidhiRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(PoojaVidhiActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
